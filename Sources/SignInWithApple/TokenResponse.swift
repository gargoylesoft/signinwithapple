import Vapor

/// The response token object returned on a successful request
/// - See: [Apple's TokenResponse doc](https://developer.apple.com/documentation/signinwithapplerestapi/tokenresponse)
public struct TokenResponse: Content, Decodable {
    /// (Reserved for future use) A token used to access allowed data. Currently, no data set has been defined for access.
    public let accessToken: String

    /// The amount of time, in seconds, before the access token expires.
    public let expiresIn: Int

    /// A JSON Web Token that contains the user’s identity information.
    public let idToken: String

    /// The refresh token used to regenerate new access tokens. Store this token securely on your server.
    public let refreshToken: String

    /// The type of access token. It will always be Bearer.
    public let tokenType: String

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case idToken = "id_token"
        case refreshToken = "refresh_token"
        case tokenType = "token_type"
    }
}
