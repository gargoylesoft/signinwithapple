import Vapor

internal struct AuthTokenBody: Encodable {
    /// The application identifier for your app.
    let clientId: String

    /// A secret generated as a JSON Web Token that uses the secret key generated by the WWDR portal.
    /// - See: `SignInWithAppleClientSecret`
    let clientSecret: String

    /// The authorization code received from your application’s user agent. The code is single use only and valid for five minutes.
    let code: String

    /// The grant type that determines how the client interacts with the server.
    let grantType = "authorization_code"

    /// The destination URI the code was originally sent to.
    let redirectUri: String

    enum CodingKeys: String, CodingKey {
        case clientId = "client_id"
        case clientSecret = "client_secret"
        case code
        case grantType = "grant_type"
        case redirectUri = "redirect_uri"
    }

    init(privateKeyId: String, privateKey: Data, clientBundleId: String, teamId: String, expirationSeconds: Int, code: String, redirectUri: String) throws {
        clientSecret = try ClientSecret.generate(privateKeyId: privateKeyId, expirationSeconds: expirationSeconds, clientBundleId: clientBundleId, teamId: teamId, privateKey: privateKey)
        clientId = clientBundleId
        self.code = code
        self.redirectUri = redirectUri
    }
}
