import Vapor
import JWTKit

/// The `identityToken` property of `ASAuthorizationAppleIDCredential`
internal struct IdentityToken: JWTPayload {
    let iss: IssuerClaim
    let aud: AudienceClaim
    let exp: ExpirationClaim
    let iat: IssuedAtClaim
    let sub: SubjectClaim

    func verify(using signer: JWTSigner) throws {
        guard iss.value == "https://appleid.apple.com" else {
            throw JWTError.claimVerificationFailure(name: "iss", reason: "Incorrect issuer")
        }

        try exp.verifyNotExpired()
    }
}

