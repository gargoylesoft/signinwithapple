import Vapor
import JWTKit

/// The header and payload for the client_secret of the `AuthTokenBody`
/// - See: [Generate and validate tokens](https://developer.apple.com/documentation/signinwithapplerestapi/generate_and_validate_tokens)
internal struct ClientSecret: JWTPayload {
    /// The issuer registered claim key, which has the value of your 10-character Team ID, obtained from your developer account.
    let iss: String

    /// The issued at registered claim key, the value of which indicates the time at which the token was generated, in terms of the number of seconds since Epoch, in UTC.
    let iat = Int(Date().timeIntervalSince1970)

    /// The expiration time registered claim key, the value of which must not be greater than 15777000 (6 months in seconds) from the Current Unix Time on the server.
    let exp: Int

    /// The audience registered claim key, the value of which identifies the recipient the JWT is intended for. Since this token is meant for Apple, use https://appleid.apple.com.
    let aud = "https://appleid.apple.com"

    /// The subject registered claim key, the value of which identifies the principal that is the subject of the JWT. Use the same value as client_id as this token is meant for your application.
    let sub: String

    private init(expirationSeconds: Int, clientBundleId: String, teamId: String) {
        iss = teamId
        exp = iat + expirationSeconds
        sub = clientBundleId
    }

    func verify(using signer: JWTSigner) throws {
        guard iss.count == 10 else {
            throw JWTError.claimVerificationFailure(name: "iss", reason: "teamId must be your 10-character Team ID from the developer portal")
        }

        let lifetime = exp - iat
        guard 0...15777000 ~= lifetime else {
            // Per Apple docs, must not be greater than this value which is 6 months in seconds
            throw JWTError.claimVerificationFailure(name: "exp", reason: "Expiration must be between 0 and 15777000")
        }
    }

    static func generate(privateKeyId: String, expirationSeconds: Int, clientBundleId: String, teamId: String, privateKey: Data) throws -> String {
        guard privateKeyId.count == 10 else {
            throw JWTError.claimVerificationFailure(name: "kid", reason: "privateKeyId must be the 10-character key identifier obtained from your developer account")
        }

        let me = ClientSecret(expirationSeconds: expirationSeconds, clientBundleId: clientBundleId, teamId: teamId)

        let header = JWTHeader(alg: "ES256", typ: nil, kid: privateKeyId)
        let jwt = JWT(header: header, payload: me)

        let key = try ECDSAKey.private(pem: privateKey)
        let signer = JWTSigner.es256(key: key)
        let bytes = try jwt.sign(using: signer)

        return String(bytes: bytes, encoding: .utf8)!
    }
}
