//
//  File.swift
//  
//
//  Created by Scott Grosch on 9/22/19.
//

import Vapor

/// Represents the input required to generate an access token from Apple
public struct AuthenticationInput: Content {
    /// The `identityToken` from `ASAuthorizationAppleIDCredential`
    public let identity: String

    /// The `authorizationCode` from `ASAuthorizationAppleIDCredential`
    public let code: String
}
