//
//  File.swift
//  
//
//  Created by Scott Grosch on 9/14/19.
//

import Foundation

public enum JWKError: Error {
    /// Internal SSL failure such as unable to create/verify a signature
    case opensslFailure

    /// Something is wrong with the identity data passed from the client
    case badIdentity

    /// The kid specified for the JWK is invalid
    case invalidKid

    /// Keys not downloaded from Apple yet.
    case keysNotDownloaded

    /// Corrupt JWK data
    case corruptJwkData

    /// The payload was tampered with
    case signatureVerifictionFailed

    /// Failed to call the `configure` method before using member methods
    case notConfigured

    /// The passed in URL was not a file based URL
    case nonFileUrl
}
