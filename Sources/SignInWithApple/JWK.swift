//
//  File.swift
//  
//
//  Created by Scott Grosch on 9/14/19.
//

import Vapor

/// Defines a single JSON Web Key used by Sign In with Apple
internal struct JWK: Decodable {
    /// The encryption algorithm used to encrypt the token
    let alg: String

    /// The exponent value for the RSA public key
    let e: String

    /// A 10-character identifier key, obtained from your developer account.
    let kid: String

    /// The key type parameter setting.  This must be set to "RSA"
    let kty: String

    /// The modulus value for the RSA public key
    let n: String

    /// The indended use for the public key
    let use: String
}

internal struct JWKSet: Decodable {
    let keys: [JWK]
}
