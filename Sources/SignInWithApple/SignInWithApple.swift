import Vapor
import COpenCrypto
import JWTKit

/// Handles the generation of access tokens and refresh tokens for Sign In with Apple
final public class SignInWithApple {
    /// The singleton you use for access.
    static public let shared = SignInWithApple()

    /// The bundle ID you set on your iOS app in Xcode.  Always call `configure` first.  This property is only made public so that if your
    /// server is supporting multiple apps you can swap the bundle ID in-between calls.
    public var clientBundleId: String?

    private var privateKeyId: String?
    private var privateKeyData: Data?
    private var teamId: String?
    private var redirectUri: String?
    private var rsaKeys: [String: RSAKey] = [:]
    private var authKeyUrl = URL(string: "https://appleid.apple.com/auth/keys")!

    private init() {}

    /// Configure the details Apple needs to successfully generate tokens
    /// - Parameter privateKeyId: The 10-character ID of the private key you downloaded when you configured Sign In with Apple.
    /// - Parameter privateKeyData: The content of the private key file you downloaded when you configured Sign In with Apple
    /// - Parameter clientBundleId: The bundle ID you set on your iOS app in Xcode
    /// - Parameter teamId: The 10-character team identifier for your Apple developer account
    /// - Parameter redirectUri: The URL to redirect to.
    public func configure(privateKeyId: String, privateKeyData: Data, clientBundleId: String, teamId: String, redirectUri: String) {
        self.privateKeyId = privateKeyId
        self.privateKeyData = privateKeyData
        self.clientBundleId = clientBundleId
        self.teamId = teamId
        self.redirectUri = redirectUri
    }

    /// Assigns the JWK data if already cached from Apple
    /// - Parameter data: The contents of the JWK Set from Apple
    public func assignKeys(data: Data) throws {
        let decoder = JSONDecoder()

        guard let jwks = try? decoder.decode(JWKSet.self, from: data) else {
            throw JWKError.corruptJwkData
        }

        var ret: [String: RSAKey] = [:]

        for jwk in jwks.keys {
            guard let rsa = try? self.generateRsaKey(for: jwk) else { continue }
            ret[jwk.kid] = rsa
        }

        self.rsaKeys = ret
    }

    /// Downloads a fresh set of keys from Apple
    /// - Parameter completion: Called when download ends with the `Data` object downloaded or `nil` on failure
    public func refreshKeys(completion: @escaping (Data?) -> Void) {
        URLSession.shared.dataTask(with: authKeyUrl) { [weak self] data, response, _ in
            guard let self = self else { return }

            let resp = response as! HTTPURLResponse
            guard resp.statusCode == 200, let data = data else {
                completion(nil)
                return
            }

            do {
                try self.assignKeys(data: data)
                completion(data)
            } catch {
                completion(nil)
            }
        }.resume()
    }

    /// Gnenerates a bearer access token that the client should use for authentication.
    /// - Parameter expirationSeconds: The lifetime of the token, in seconds.
    /// - Parameter code: The `authorizationCode` from `ASAuthorizationAppleIDCredential`
    /// - Parameter client: The `Client` connection to make requests against.
    /// - See: [ASAuthorizationAppleIDCredential](https://developer.apple.com/documentation/authenticationservices/asauthorizationappleidcredential)
    public func generateAccessToken(expirationSeconds: Int, code: String, client: Client) throws -> EventLoopFuture<TokenResponse> {
        guard let privateKeyData = privateKeyData, let privateKeyId = privateKeyId, let clientBundleId = clientBundleId, let teamId = teamId, let redirectUri = redirectUri else {
            throw JWKError.notConfigured
        }

        let payload = try AuthTokenBody(privateKeyId: privateKeyId, privateKey: privateKeyData, clientBundleId: clientBundleId, teamId: teamId, expirationSeconds: expirationSeconds, code: code, redirectUri: redirectUri)

        return try getToken(client: client, payload: payload)
    }

    /// Generates a new bearer access token based on an existing refresh token.
    /// - Parameter expirationSeconds: The lifetime of the token, in seconds.
    /// - Parameter code: The `authorizationCode` from `ASAuthroizationAppleIDCredential`
    /// - Parameter client: The `Client` connection to make requests against.
    /// - Parameter refreshToken: The refresh token received from the previous token generation call.
    public func refreshAccessToken(expirationSeconds: Int, code: String, client: Client, refreshToken: String) throws -> EventLoopFuture<TokenResponse> {
        guard let privateKeyData = privateKeyData, let privateKeyId = privateKeyId, let clientBundleId = clientBundleId, let teamId = teamId else {
            throw JWKError.notConfigured
        }

        let payload = try RefreshTokenBody(privateKeyId: privateKeyId, privateKey: privateKeyData, clientBundleId: clientBundleId, teamId: teamId, expirationSeconds: expirationSeconds, code: code, refreshToken: refreshToken)

        return try getToken(client: client, payload: payload)
    }

    /// Validates that the identity provided from the client-side Sign In with Apple is authentic.
    /// - Parameter identity: The `identityToken` from `ASAuthorizationAppleIDCredential`
    /// - See: [ASAuthorizationAppleIDCredential](https://developer.apple.com/documentation/authenticationservices/asauthorizationappleidcredential)
    public func validateIdentityToken(identity: String) throws {
        guard let data = identity.data(using: .utf8) else {
            throw JWKError.badIdentity
        }

        let jwt = try JWT<IdentityToken>(fromUnverified: data)

        guard let kid = jwt.header.kid else {
            throw JWKError.badIdentity
        }

        guard jwt.payload.aud.value == clientBundleId else {
            throw JWKError.badIdentity
        }

        let result = signerFor(kid: kid)
        switch result {
        case .failure(let err):
            throw err

        case .success(let signer):
            // TANNER: I don't like that I'm creating the JWT twice...is there a way to verify
            // signature of an already created JWT directly?
            guard (try? JWT<IdentityToken>(from: data, verifiedBy: signer)) != nil else {
                throw JWKError.signatureVerifictionFailed
            }
        }
    }

    // MARK: - Private Interface
    private func generateRsaKey(for jwk: JWK) throws -> RSAKey {
        guard let rsaKey = RSA_new() else {
            throw JWKError.opensslFailure
        }

        // defer { RSA_free(rsaKey) } - Swift will abort if you call this.  Why?

        rsaKey.pointee.n = jwk.n.urlSafeBase64Decode()?.toBigNum()
        rsaKey.pointee.e = jwk.e.urlSafeBase64Decode()?.toBigNum()

        guard let key = EVP_PKEY_new() else {
            throw JWKError.opensslFailure
        }

        defer { EVP_PKEY_free(key) }

        guard EVP_PKEY_assign(key, EVP_PKEY_RSA, rsaKey) > 0 else {
            throw JWKError.opensslFailure
        }

        guard let bio = BIO_new(BIO_s_mem()) else {
            throw JWKError.opensslFailure
        }

        defer { BIO_free(bio) }

        guard PEM_write_bio_PUBKEY(bio, key) == 1 else {
            throw JWKError.opensslFailure
        }

        let keyLen = BIO_ctrl(bio, BIO_CTRL_PENDING, 0, nil)

        guard keyLen > 0 else {
            throw JWKError.opensslFailure
        }

        let publicKey = UnsafeMutablePointer<UInt8>.allocate(capacity: keyLen)
        guard BIO_read(bio, publicKey, Int32(keyLen)) > 0 else {
            throw JWKError.opensslFailure
        }

        let pem = Data(bytes: publicKey, count: keyLen)
        return try RSAKey.public(pem: pem)
    }

    private func signerFor(kid: String) -> Result<JWTSigner, JWKError> {
        guard rsaKeys.count > 0 else {
            return .failure(JWKError.keysNotDownloaded)
        }

        guard let key = rsaKeys[kid] else {
            return .failure(JWKError.invalidKid)
        }

        let signer = JWTSigner.rs256(key: key)
        return .success(signer)
    }

    private func getToken<T: Encodable>(client: Client, payload: T) throws -> EventLoopFuture<TokenResponse> {
        let headers = HTTPHeaders(dictionaryLiteral: ("Content-Type", HTTPMediaType.urlEncodedForm.serialize()))
        let uri = URI(string: "https://appleid.apple.com/auth/token")
        let post = client.post(uri, headers: headers) { request in
            try request.content.encode(payload, as: .urlEncodedForm)
        }

        return post.flatMapThrowing { response -> TokenResponse in
            try response.content.decode(TokenResponse.self)
        }
    }
}
