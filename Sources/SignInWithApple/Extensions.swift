//
//  Extensions.swift
//  
//
//  Created by Scott Grosch on 9/14/19.
//

import Foundation
import COpenCrypto

internal extension String {
    func urlSafeBase64Decode() -> Data? {
        let len = ((count + 3) / 4) * 4

        let str = padding(toLength: len, withPad: "=", startingAt: 0)
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")

        return Data(base64Encoded: str)
    }
}

internal extension Data {
    func toBigNum() -> UnsafeMutablePointer<BIGNUM> {
        [UInt8](self).withUnsafeBufferPointer { p in
            BN_bin2bn(p.baseAddress, Int32(p.count), nil)
        }
    }
}
