import XCTest

import SignInWithAppleTests

var tests = [XCTestCaseEntry]()
tests += SignInWithAppleTests.allTests()
XCTMain(tests)
