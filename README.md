# This is still in development **do not use this yet**!
# Requires Vapor 4


## Modify boot.swift

In your *boot.swift* file call `SignInWithApple.shared.refreshKeys(completion:)` to download the current set of keys from Apple.  A `Data?` is returned, containing the contents of the keys downloaded.  

## Modify routes.swift

Add a `client: c.make()` parameter to the constructor of your controller which will handle authentication.  `Client` is a new object in Vapor 4 which will be used to make HTTP calls.  Store the client object as a property of your controller.

## Setup your controller

### Configure Sign In with Apple

In your controller's constructor you'll want to read your private key file that you downloaded from Apple and then configure based on your details.

```swift
private let client: Client

init(client: Client) {
    self.client = client

    // Obviously read this from somewhere safe in real code.
    let privateKeyData = "-----BEGIN PRIVATE KEY ... -----END PRIVATE KEY-----".data(using: .utf8)!

    SignInWithApple.shared.configure(privateKeyId: "QWERTYUIOP", privateKeyData: privateKeyData,
                                     clientBundleId: "com.yourcompany.yourapp", teamId: "ABCDEFGHIJ",
                                     redirectUri: "")
}
```

### Create a POST route

Create a route you can POST to that will validate the input token and generate the access token

```swift
func authenticate(req: Request) throws -> EventLoopFuture<String> {
    let data = try req.content.decode(AuthenticationInput.self)

    try SignInWithApple.shared.validateIdentityToken(identity: data.identity)
    return try SignInWithApple.shared.generateAccessToken(expirationSeconds: 60, code: data.code, client: client).map { token in
        // Store the refresh token in your database
        return token.accessToken
    }
}
```

Create another route you can POST to that will generate a new access token - TODO: This is really wrong...should have some type of middleware that does this automatically

```swift
func updateAuthentication(req: Request) throws -> EventLoopFuture<String> {
    let refreshToken = <read token from database>
    
    let code = try req.content.decode(String.self)
    return try SignInWithApple.shared.refreshAccessToken(expirationSeconds: 60, code: code, client: client, refreshToken: refreshToken) { token in
        return token.accessToken
    }
}
```
